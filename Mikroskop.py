import cv2
import pandas as pd
from utils.microscope import *
import os
import math
import numpy as np

# threshold_Area dubiose Fritten = 250
threshold_Area = 25
threshold_convexityDefects = 1
threshold_radii_equality = 20
threshold_circle_proximity = 10


file = '/Users/Sebastian/tubCloud/Uni/Masterarbeit Umweltchemie/Bilder/Mikroskop/Lisa/10xzoom_raindop_6ymPS.png'

image = cv2.imread(file)

binary = Convert2Binary(image)

contours, hierarchy = cv2.findContours(binary, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)[-2:]
overlaps, singlebubbles = getOverlappingBubbleContours(contours, threshold_Area,
                                                       threshold_convexityDefects)
for i in singlebubbles:
    color = (0, 255, 0)
    findAndDrawEllipse(i, image, color)

bubbles, circles, recombinated = getoverlappingBubblesSegmentation(overlaps, image,
                                                                   threshold_convexityDefects,
                                                                   threshold_radii_equality,
                                                                   threshold_circle_proximity)
for bubble in bubbles:
    color = (255, 0, 0)
    findAndDrawBubble(bubble, image, color)

for rec in recombinated:
    MatchType = rec[2]
    rec = rec[0]
    val, (p, k), rad = getLeastSquareCircle(rec)
    if not val:
        print("least squares circle not successful")
    center = (int(p), int(k))
    rad = int(rad)
    reconstructedCircleArclenght = 2 * math.pi * rad

    color = (255, 0, 255)
    if MatchType == "Ellipse":
        findAndDrawBubble(rec, image, color)
    else:
        findAndDrawBubble(rec, image, color)
binary = cv2.cvtColor(binary, cv2.COLOR_GRAY2BGR)
result = np.vstack((image, binary))

cv2.imwrite('result.png', image)
cv2.imshow('result', result)
cv2.waitKey(0)
# import the necessary packages
from scipy.spatial import distance as dist
from collections import OrderedDict
import numpy as np
import pandas as pd
from random import randint
import cv2
from datetime import datetime
from utils.math import getHalfSphereVolume, getDiameterofHalfSphere, \
    equivalentDiameterArea


# Centroid Tracker Class is based on proposed solution by Adrian Rosebrock:
# as published on 23.07.2018, accessed 10.09.2020
# https://www.pyimagesearch.com/2018/07/23/simple-object-tracking-with-opencv/
# Several adjustments and additions have been made

class CentroidTracker():
    def __init__(self, maxDisappeared=10, maxJumpDistance=10, minRegister=4,
                 trackerType='surface'):
        # initialize the next unique object ID along with three ordered
        # dictionaries used to keep track of mapping a given object
        # ID to its centroid, diameter and number of consecutive frames it has
        # been marked as "disappeared"
        self.type = trackerType
        self.nextObjectID = 0

        # Objects store centroid coordinates by ID
        self.objects = OrderedDict()
        self.objectsFrameCount = OrderedDict()

        # Last position and estimatedPosition to make tracking sensitive to
		# object movement
        self.lastPosition = OrderedDict()
        self.estimatedPosition = OrderedDict()
        self.ghostLanes = OrderedDict()

        # Disappeared stores number of consecutive frames object has not been
		# matched / found
        self.disappeared = OrderedDict()

        # Stores object diameters - Diameter
        self.diameters = OrderedDict()
        self.lastDiameters = OrderedDict()
        self.colors = OrderedDict()

        self.mergedBubbles = OrderedDict()
        self.mergeGroupIndex = OrderedDict()
        self.lastMergeGroupIndex = OrderedDict()
        self.xycoords = OrderedDict()

        self.burstperFrame = 0

        # store the number of maximum consecutive frames a given
        # object is allowed to be marked as "disappeared" and deregistered
        self.maxDisappeared = maxDisappeared

        # To avoid Jumping Tracking for disappeared objects that are replaced
		# with new ones
        self.maxJumpDistance = maxJumpDistance

        # To reduce influence of short term noise in Burst Diameters
        self.minRegister = minRegister

        # Store the number of processed frames to index trajectories
        self.analyzedFrames = 0
        self.trajectories = {}
        self.burstDiameters = []
        self.allDiameters = []

    def register(self, centroid, diameter, mergeID=0):
        # when registering an object we use the next available object
        # ID to store the centroid
        self.mergedBubbles[self.nextObjectID] = False
        self.objects[self.nextObjectID] = centroid
        self.ghostLanes[self.nextObjectID] = self.objects[self.nextObjectID]
        self.disappeared[self.nextObjectID] = 0
        self.diameters[self.nextObjectID] = diameter
        self.xycoords[self.nextObjectID] = {
            'x': round(centroid[0] * self.conversionFactor, 2), 'y': round(
                (abs(centroid[1] - self.y_ZeroOffset) * self.conversionFactor),
                2)}
        self.colors[self.nextObjectID] = (
        randint(0, 255), randint(0, 255), randint(0, 255))
        self.mergeGroupIndex[self.nextObjectID] = mergeID
        self.objectsFrameCount[self.nextObjectID] = 1
        self.nextObjectID += 1

    def setCalibrationParameters(self, conversionFactor, y_ZeroOffset=0):
        self.conversionFactor = conversionFactor
        self.y_ZeroOffset = y_ZeroOffset

    def deregister(self, objectID):
        # Write diameter at burst to list,
        # then delete the object ID from all dictionaries

        if self.type == 'surface':
            mergeDetected = False
            if objectID in self.lastMergeGroupIndex:
                mergeGroup = self.lastMergeGroupIndex[objectID]
                if mergeGroup != 0:
                    for key, value in self.lastMergeGroupIndex.items():
                        if value == mergeGroup:
                            sizeDiff = self.diameters[key] - self.lastDiameters[
                                key]
                            sizeDiffThreshold = 0.25 * (getDiameterofHalfSphere(
                                (getHalfSphereVolume(
                                    self.diameters[key]) + getHalfSphereVolume(
                                    self.lastDiameters[objectID]))[0]) -
                                                        self.diameters[key])
                            if sizeDiff > 0 and sizeDiff > sizeDiffThreshold:
                                # print('Merge Detected')
                                mergeDetected = True
            if not mergeDetected and self.objectsFrameCount[
                objectID] >= self.minRegister:
                self.burstperFrame += 1
                self.burstDiameters.append(self.diameters[objectID].copy())
        del self.ghostLanes[objectID]
        del self.objects[objectID]
        del self.disappeared[objectID]
        del self.diameters[objectID]
        del self.colors[objectID]
        del self.xycoords[objectID]
        if self.type == 'surface':
            if mergeDetected:
                self.mergedBubbles[objectID] = True
            del self.lastMergeGroupIndex[objectID]
            del self.mergeGroupIndex[objectID]
        del self.objectsFrameCount[objectID]

    def exportMergeBubbles(self, filename, path):
        df = pd.DataFrame(self.mergedBubbles, index = [0])
        df.to_csv(path + filename)

    def updateTrajectories(self):
        if len(self.objects) > 0:
            self.trajectories[self.analyzedFrames] = self.xycoords.copy()

    def storeLastPosition(self):
        self.lastPosition = self.ghostLanes.copy()

    def storeLastDiameters(self):
        self.lastDiameters = self.diameters.copy()

    def storeLastMergeGroupIDs(self):
        self.lastMergeGroupIndex = self.mergeGroupIndex.copy()

    def estimatePosition(self):
        self.estimatedPosition = {k: (np.array([self.ghostLanes[k][0] + (
                    self.ghostLanes[k][0] - self.lastPosition[k][0]),
                                                self.ghostLanes[k][1] + (
                                                            self.ghostLanes[k][
                                                                1] -
                                                            self.lastPosition[
                                                                k][
                                                                1])]) if k in self.lastPosition else
                                      self.ghostLanes[k]) for k in
                                  self.ghostLanes}

    def concatTrajectories(self):
        print('Make Dict of Dataframes: ', datetime.now())
        # dict_of_df = {k: pd.DataFrame(v) for k,v in self.trajectories.items()}
        dict_of_df = {(outerKey, innerKey): values for outerKey, innerDict in
                      self.trajectories.items() for innerKey, values in
                      innerDict.items()}
        print('Concat DF of DFs: ', datetime.now())
        df = pd.DataFrame.from_dict(dict_of_df).stack(0)
        print('Set Indexes ', datetime.now())
        # df.index.names = ['Frame', 'Coordinate']
        df.index.names = ['Coordinate', 'Frame']
        return df

    def exportTrajectories(self, filename, path):
        print('Trajectories')
        print('start concat: ', datetime.now())
        concatenated = self.concatTrajectories()
        print('write pickle: ', datetime.now())
        #concatenated.to_pickle(path + filename.replace('.csv', '.pick'))
        print('write csv: ', datetime.now())
        concatenated.to_csv(path + filename)

    def updateDiameters(self):
        if len(self.objects) > 0:
            self.allDiameters.append(self.diameters.copy())

    def concatDiameters(self):
        return pd.DataFrame(self.diameters)

    def concatAllDiameters(self):
        return pd.DataFrame(self.allDiameters).astype(float)

    def exportDiameters(self, filename, path, conversionFactor=1):
        (self.concatAllDiameters() * conversionFactor).to_csv(path + filename)

    def drawObjectContours(self, image):
        cv2.drawContours(image, cnt, -1, (
        random.randint(0, 255), random.randint(0, 255), random.randint(0, 255)),
                         2, cv2.FILLED)

    def concatBurstDiameters(self):
        return pd.DataFrame(self.burstDiameters)

    def exportBurstDiameters(self, filename, path, conversionFactor=1):
        (self.concatBurstDiameters() * conversionFactor).to_csv(path + filename)

    def update(self, rects):
        self.analyzedFrames += 1
        self.burstperFrame = 0
        if not self.lastPosition:
            self.lastPosition = self.objects.copy()

        # check to see if the list of input bounding box rectangles is empty
        if len(rects) == 0:
            # loop over any existing tracked objects and mark them as
			# disappeared
            for objectID in list(self.disappeared.keys()):
                self.disappeared[objectID] += 1

                # deregister if object disappeared for more frames than
				# maxDisappeared
                if self.disappeared[objectID] > self.maxDisappeared:
                    self.deregister(objectID)

            # return (no features to extract from empty list)
            return self.objects

        # initialize an array of input centroids and diameters for the
		# current frame
        inputCentroids = np.zeros((len(rects), 2), dtype="int")
        inputDiameters = np.zeros((len(rects), 1), dtype="int")
        inputMergeGroupIDs = np.zeros((len(rects), 1), dtype="int")

        if self.type == 'surface':
            # loop over the bounding box rectangles
            for (i, (startX, startY, endX, endY, groupID, area)) in enumerate(
                    rects):
                # use the bounding box coordinates to derive the centroid and
				# diameter
                cX = int((startX + endX) / 2.0)
                cY = int((startY + endY) / 2.0)
                inputCentroids[i] = (cX, cY)
                inputDiameters[i] = min(endX - startX, endY - startY)
                inputMergeGroupIDs[i] = groupID

            # if not tracking any objects take the input data and register
			# each of them
            if len(self.objects) == 0:
                for i in range(0, len(inputCentroids)):
                    self.register(inputCentroids[i], inputDiameters[i],
                                  inputMergeGroupIDs[i])

            # if tracking, try to match the input centroids to existing
			# object centroids
            else:
                # grab the set of object IDs and corresponding centroids
                objectIDs = list(self.objects.keys())
                objectCentroids = list(self.objects.values())

                # compute the distance between each pair of object centroids
				# and input centroids
                D = dist.cdist(np.array(objectCentroids), inputCentroids)

                # (1) find the smallest value in each row
                # (2) sort the row indexes based on their minimum values
                # (smallest value at the *front* of the index list
                rows = D.min(axis=1).argsort()

                # similar process on the columns
                cols = D.argmin(axis=1)[rows]

                # in order to determine if we need to update, register,
				# or deregister an object, keep track of which of the rows and
				# column indexes are already examined
                usedRows = set()
                usedCols = set()

                # loop over the combination of the (row, column) index tuples
                for (row, col) in zip(rows, cols):
                    # if already examined either row or column before, ignore it
                    if row in usedRows or col in usedCols:
                        continue

                    # otherwise, grab object ID for the current row, set new
					# centroid and reset the disappeared counter
                    if int(D[row, col]) < self.maxJumpDistance:
                        objectID = objectIDs[row]
                        self.objects[objectID] = inputCentroids[col]
                        self.diameters[objectID] = inputDiameters[col]
                        self.disappeared[objectID] = 0
                        self.mergeGroupIndex[objectID] = inputMergeGroupIDs[col]
                        self.objectsFrameCount[objectID] += 1
                        self.ghostLanes[objectID] = self.objects[objectID]

                        # indicate that each of the row and column indexes
						# were examined
                        usedRows.add(row)
                        usedCols.add(col)

                # compute both the row and column index NOT yet examined
                unusedRows = set(range(0, D.shape[0])).difference(usedRows)
                unusedCols = set(range(0, D.shape[1])).difference(usedCols)

                # if number of object centroids is equal or greater than
				# number of input centroids check and see if some of these
				# objects have potentially disappeared
                if D.shape[0] >= D.shape[1]:
                    # loop over the unused row indexes
                    for row in unusedRows:
                        # grab the object ID for the corresponding row
                        # index and increment the disappeared counter
                        objectID = objectIDs[row]
                        self.disappeared[objectID] += 1
                        self.ghostLanes[objectID] = self.estimatedPosition[
                            objectID]

                        # check to see if object should be deregistered
                        if self.disappeared[objectID] > self.maxDisappeared:
                            self.deregister(objectID)

                # if number of input centroids is greater than the number of
				# existing object centroids
                # register each new input centroid as a trackable object
                else:
                    for col in unusedCols:
                        self.register(inputCentroids[col], inputDiameters[col],
                                      inputMergeGroupIDs[col])

            # return the set of trackable objects
            return self.objects

        if self.type == 'droplets':
            # loop over the bounding box rectangles
            for (i, (startX, startY, endX, endY, diam)) in enumerate(rects):
                # use the bounding box coordinates to derive the centroid and
				# diameter
                cX = int((startX + endX) / 2.0)
                cY = int((startY + endY) / 2.0)
                inputCentroids[i] = (cX, cY)
                inputDiameters[i] = diam

            # if not tracking any objects take the input data and register
			# each of them
            if len(self.objects) == 0:
                for i in range(0, len(inputCentroids)):
                    self.register(inputCentroids[i], inputDiameters[i])

            # if tracking, try to match the input centroids to existing
			# object centroids
            else:
                # grab the set of object IDs and corresponding centroids
                objectIDs = list(self.estimatedPosition.keys())
                objectCentroids = list(self.estimatedPosition.values())

                # compute the distance between each pair of object centroids
				# and input centroids
                D = dist.cdist(np.array(objectCentroids), inputCentroids)

                # (1) find the smallest value in each row
                # (2) sort the row indexes based on their minimum values
                # (smallest value at the *front* of the index list
                rows = D.min(axis=1).argsort()

                # similar process on the columns
                cols = D.argmin(axis=1)[rows]

                # in order to determine if we need to update, register,
				# or deregister an object, keep track of which of the rows
				# and column indexes are already examined
                usedRows = set()
                usedCols = set()

                # loop over the combination of the (row, column) index tuples
                for (row, col) in zip(rows, cols):
                    # if already examined either row or column before, ignore it
                    if row in usedRows or col in usedCols:
                        continue

                    # otherwise, grab object ID for the current row, set new
					# centroid and reset the disappeared counter
                    if int(D[row, col]) < self.maxJumpDistance:
                        objectID = objectIDs[row]
                        self.objects[objectID] = inputCentroids[col]
                        self.diameters[objectID] = inputDiameters[col]
                        self.disappeared[objectID] = 0
                        self.ghostLanes[objectID] = self.objects[objectID]
                        self.objectsFrameCount[objectID] += 1
                        self.xycoords[objectID] = {'x': round(
                            inputCentroids[col][0] * self.conversionFactor, 2),
                                                   'y': round((abs(
                                                       inputCentroids[col][
                                                           1] - self.y_ZeroOffset) * self.conversionFactor),
                                                              2)}

                        # indicate that each of the row and column indexes
						# were examined
                        usedRows.add(row)
                        usedCols.add(col)

                # compute both the row and column index NOT yet examined
                unusedRows = set(range(0, D.shape[0])).difference(usedRows)
                unusedCols = set(range(0, D.shape[1])).difference(usedCols)

                # if number of object centroids is equal or greater than
				# number of input centroids check and see if some of these
				# objects have potentially disappeared
                if D.shape[0] >= D.shape[1]:
                    # loop over the unused row indexes
                    for row in unusedRows:
                        # grab the object ID for the corresponding row
                        # index and increment the disappeared counter
                        objectID = objectIDs[row]
                        self.disappeared[objectID] += 1

                        # check to see if object should be deregistered
                        if self.disappeared[objectID] > self.maxDisappeared:
                            self.deregister(objectID)

                # if number of input centroids is greater than the number of
				# existing object centroids
                # register each new input centroid as a trackable object
                else:
                    for col in unusedCols:
                        self.register(inputCentroids[col], inputDiameters[col])

            # return the set of trackable objects
            return self.objects

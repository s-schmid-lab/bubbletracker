def FindContours(binary):
    contours, hierarchy = cv2.findContours(binary, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    return contours, hierarchy

def splitContourIterative(cnt, defectslist, threshold_convexityDefects, threshold_Area, image, maxIterations):

    # This function loops over the defects list and extracts one bubble for each iteration, until there is only one left
    cntRemainder = cnt
    bubblediameters = []
    iteration = 0
    drawlastcontour = True

    while len(defectslist) > 1 and cv2.contourArea(cntRemainder) > threshold_Area:
        # Split first bubble from contour
        splitBubbleCnt = np.append(cntRemainder[:defectslist[0]], cntRemainder[defectslist[-1]:], axis=0)

        # New Contour without first bubble
        cntRemainder = cntRemainder[defectslist[0]:defectslist[-1]]

        # Draw bubble and find diameter
        bubblediameters.append(findAndDrawEllipse(splitBubbleCnt, image))

        # Find new hull and new convexity Defects
        try:
            hull = cv2.convexHull(cntRemainder, returnPoints=False)
            defects = cv2.convexityDefects(cntRemainder, hull)
            defectslist = getRelevantDefects(defects, threshold_convexityDefects)
        except:
            print("Error in handling convexityDefects, contour is not monotonic")
            cv2.drawContours(image, cntRemainder, -1, (0, 0, 255), 5)
            drawlastcontour = False
            break

        # Emergency Stop
        iteration += 1
        if iteration > maxIterations:
            print("Iterations in contour splitting exceeded maximum value of " + str(maxIterations))
            break

    # All Intersections are resolved, now its time to draw the last bubble
    if drawlastcontour:
        bubblediameters.append(findAndDrawEllipse(cntRemainder, image))

    return bubblediameters

def splitOverlaps(contours, image, threshold_convexityDefects, threshold_Area, maxIterations):
    splitContours = []
    mergeCandidates = []

    for cnt in contours:
        area = cv2.contourArea(cnt)

        # Filter out unwanted artefacts (dirt, etc.)
        if area > threshold_Area:
            try:
                hull = cv2.convexHull(cnt, returnPoints=False)
                defects = cv2.convexityDefects(cnt, hull)
            except:
                print("Error in handling convexityDefects, contour is not monotonic")
                defects = None

            # Check for defects
            if defects is not None and len(cnt) > 5:
                defectsList = getRelevantDefects(defects, threshold_convexityDefects)

                # Combined Circles contours have at least 2 defects
                if len(defectsList) >= 2:

                    # Mark combined Circles in Image
                    x, y, w, h = cv2.boundingRect(cnt)
                    cv2.rectangle(image, (x, y), (x + w, y + h), (0, 0, 255), 2)

                    # Split Contour on Convexity Defects, Defects are considered to either of the touching contour
                    #overlappingContours = splitContourAtDefects(cnt, defectsList, threshold_convexityDefects, threshold_Area, image, 4)

                    overlappingContours = getSplitPoints(cnt, defectsList, threshold_convexityDefects, threshold_Area, image)

                    for subcnt in overlappingContours:
                        splitContours.append(subcnt)
                        mergeCandidates.append(subcnt)

                # Only one defect indicates one bubble
                else:
                    splitContours.append(cnt)
            else:
                splitContours.append(cnt)
    return splitContours, mergeCandidates

def splitContourAtDefects(cnt, defectslist, threshold_convexityDefects, threshold_Area, image, maxIterations):

    # This function loops over the defects list and extracts one bubble for each iteration, until there is only one left
    cntRemainder = cnt
    iteration = 0
    drawlastcontour = True

    # Initialize list to store individual contours after splitting
    splitContours = []

    while len(defectslist) > 1 and cv2.contourArea(cntRemainder) > threshold_Area:

        # Split first bubble from contour
        splitBubbleCnt = np.append(cntRemainder[:defectslist[0]], cntRemainder[defectslist[-1]:], axis=0)

        # New Contour without first bubble
        cntRemainder = cntRemainder[defectslist[0]:defectslist[-1]]

        # Repair both contours
        splitBubbleCnt = cv2.approxPolyDP(splitBubbleCnt, 0.1, False)
        cntRemainder = cv2.approxPolyDP(cntRemainder, 2, False)

        # Append to contour list for export
        splitContours.append(splitBubbleCnt)

        # Find new hull and new convexity Defects
        try:
            hull = cv2.convexHull(cntRemainder, returnPoints=False)
            defects = cv2.convexityDefects(cntRemainder, hull)
            defectslist = getRelevantDefects(defects, threshold_convexityDefects)

        except:
            print("Error in handling convexityDefects, contour is not monotonic")
            cv2.drawContours(image, cntRemainder, -1, (0, 0, 255), 5)
            drawlastcontour = False
            break

        # Emergency Stop
        iteration += 1
        if iteration > maxIterations:
            print("Iterations in contour splitting exceeded maximum value of " + str(maxIterations))
            break

    # All Intersections are resolved, now its time to draw the last bubble
    if drawlastcontour:
        splitContours.append(cntRemainder)
    return splitContours

def getSplitPoints(contour, defects, threshold_convexityDefects, threshold_Area, image):
    BubbbleContours = []

    remainingContour = contour
    remainingContourDefects = defects
    count_iterations = 0

    # as long as there are defects in the remaining Contour
    while len(remainingContourDefects) > 1 and cv2.contourArea(remainingContour) > threshold_Area:

        # Split contour at global maximal defects for each hull line and extract first segment
        segment = np.append(remainingContour[remainingContourDefects[-1]:], remainingContour[:remainingContourDefects[0]], axis=0)
        remainingContour = remainingContour[remainingContourDefects[0]:remainingContourDefects[-1]]

        # Check if first segment and remaininContour contain local maxima defects, which were overridden by global defect
        try:
            segmentHull = cv2.convexHull(segment, returnPoints=False)
            segmentDefects = getRelevantDefects(cv2.convexityDefects(segment, segmentHull), threshold_convexityDefects)
        except:
            print("Could not get hull for segment")
            print(segment)
            break
        # If there are more defects in segment, do further splitting
        while len(segmentDefects) > 2 and cv2.contourArea(segment) > threshold_Area and len(segment) > 5:

            # segment = np.append(segment[:segmentDefects[0]], segment[segmentDefects[-1]:], axis=0)
            segment = segment[:segmentDefects[0]]

            remainingContour = np.insert(remainingContour, 0, segment[segmentDefects[0]:], axis=0)

            # Check if segment contains local maxima defects, which were overridden by previous global defect
            try:
                segmentHull = cv2.convexHull(segment, returnPoints=False)
                segmentDefects = getRelevantDefects(cv2.convexityDefects(segment, segmentHull), threshold_convexityDefects)
            except:
                segmentDefects = []
                print("Could not get hull for segment")
                cv2.drawContours(image, [segment], 0, (0, 255, 0), 3)
                print(segment)

            count_iterations += 1

            if count_iterations > 15:
                print("Too many iterations in segment splitting")
                try:
                    cv2.drawContours(image, [remainingContour], 0, (0, 255, 0), 3)
                except:
                    pass
                break

        # Escape Loop when there is no more defects
        BubbbleContours.append(segment)

        # Calculate new defects
        if len(remainingContour) > 5:
            try:
                remainingContourHull = cv2.convexHull(remainingContour, returnPoints=False)
                remainingContourDefects = getRelevantDefects(cv2.convexityDefects(remainingContour, remainingContourHull),
                                                        threshold_convexityDefects)
            except:
                print("Could not get hull for remaining contour")
                print(remainingContour)
                cv2.drawContours(image, [remainingContour], 0, (0, 255, 0), 3)
                remainingContourDefects = []
                break
        else:
            remainingContourDefects = []

    BubbbleContours.append(remainingContour)
    return BubbbleContours

def analyzeContours(contours, image, threshold_Area, threshold_convexityDefects):
    diameters = []
    for cnt in contours:
        area = cv2.contourArea(cnt)

        # Filter out unwanted artefacts (dirt, etc.)
        if area > threshold_Area:
            hull = cv2.convexHull(cnt, returnPoints=False)
            defects = cv2.convexityDefects(cnt, hull)

            # Check for defects
            if defects is not None:
                defectsList = getRelevantDefects(defects, threshold_convexityDefects)

                # Combined Circles contours have at least 2 defects
                if len(defectsList) >= 2:

                    # Mark combined Circles in Image
                    x, y, w, h = cv2.boundingRect(cnt)
                    cv2.rectangle(image, (x, y), (x + w, y + h), (0, 0, 255), 2)

                    # Split Contour on Convexity Defects, Defects are considered to either of the touching contour
                    splitBubbleDiameters = splitContourIterative(cnt, defectsList, threshold_convexityDefects, threshold_Area, image, 4)
                    diameters.extend(splitBubbleDiameters)

                # Only one bubble
                else:
                    diameter = findAndDrawEllipse(cnt, image)
                    diameters.append(diameter)

    return diameters

def analyzeContours2(contours, image, threshold_Area, maxDiameter):
    diameters = []
    for cnt in contours:
        area = cv2.contourArea(cnt)
        # Filter out unwanted artefacts (dirt, etc.)
        if area > threshold_Area:
            diameter = findAndDrawEllipse(cnt, image)
            if diameter < maxDiameter:
                diameters.append(diameter)
    return diameters

def getRelevantDefectsNeu(rawDefects, threshold_convexityDefects):
    defectslist = []
    if rawDefects is not None:
        for i in range(rawDefects.shape[0]):
         # Get start, end, far, distance for each defect, only distance and far points are used
            s, e, f, d = rawDefects[i, 0]

            # Only major defects ( >= convexityDefectsThreshold) are significant for combined bubbles
            if d >= threshold_convexityDefects:
                defectslist.append(rawDefects[i, 0])
    return defectslist


class CentroidTracker():
	def __init__(self, maxDisappeared=10):
		# initialize the next unique object ID along with three ordered
		# dictionaries used to keep track of mapping a given object
		# ID to its centroid, diameter and number of consecutive frames it has
		# been marked as "disappeared"
		self.nextObjectID = 0

		self.objects = OrderedDict()
		self.lastPosition = OrderedDict()
		self.estimatedPosition = OrderedDict()
		self.disappeared = OrderedDict()
		self.diameters = OrderedDict()
		self.colors = OrderedDict()

		# store the number of maximum consecutive frames a given
		# object is allowed to be marked as "disappeared" and deregistered
		self.maxDisappeared = maxDisappeared

		# Store the number of processed frames to index trajectories
		self.analyzedFrames = 0
		self.trajectories = []
		self.allDiameters = []

	def register(self, centroid, diameter):
		# when registering an object we use the next available object
		# ID to store the centroid
		self.objects[self.nextObjectID] = centroid
		self.disappeared[self.nextObjectID] = 0
		self.diameters[self.nextObjectID] = diameter
		self.colors[self.nextObjectID] = (randint(0, 255), randint(0, 255), randint(0, 255))
		self.nextObjectID += 1


	def deregister(self, objectID):
		# delete the object ID from all dictionaries
		del self.objects[objectID]
		del self.disappeared[objectID]
		del self.diameters[objectID]
		del self.colors[objectID]

	def updateTrajectories(self):
		if len(self.objects) > 0:
			self.trajectories.append(self.objects.copy())

	def storeLastPosition(self):
		self.lastPosition = self.objects.copy()

	def estimatePosition(self):
		self.estimatedPosition = {k:(np.array([self.objects[k][0] + (self.objects[k][0] - self.lastPosition[k][0]), self.objects[k][1] + (self.objects[k][1] - self.lastPosition[k][1])]) if k in self.lastPosition else self.objects[k]) for k in self.objects }

	def concatTrajectories(self):
			return pd.DataFrame(self.trajectories)

	def exportTrajectories(self, filename, path):
			self.concatTrajectories().to_csv(path + filename)

	def updateDiameters(self):
		if len(self.objects) > 0:
			self.allDiameters.append(self.diameters.copy())

	def concatDiameters(self):
		return pd.DataFrame(self.diameters)

	def concatAllDiameters(self):
		return pd.DataFrame(self.allDiameters).astype(float)

	def exportDiameters(self, filename, path, conversionFactor=1):
		(self.concatAllDiameters() * conversionFactor).to_csv(path + filename)

	def update(self, rects):
		self.analyzedFrames += 1

		# check to see if the list of input bounding box rectangles is empty
		if len(rects) == 0:
			# loop over any existing tracked objects and mark them as disappeared
			for objectID in list(self.disappeared.keys()):
				self.disappeared[objectID] += 1

				# deregister if object disappeared for more frames than maxDisappeared
				if self.disappeared[objectID] > self.maxDisappeared:
					self.deregister(objectID)

			# return (no features to extract from empty list)
			return self.objects

		# initialize an array of input centroids and diameters for the current frame
		inputCentroids = np.zeros((len(rects), 2), dtype="int")
		inputDiameters = np.zeros((len(rects), 1), dtype="int")

		# loop over the bounding box rectangles
		for (i, (startX, startY, endX, endY)) in enumerate(rects):
			# use the bounding box coordinates to derive the centroid and diameter
			cX = int((startX + endX) / 2.0)
			cY = int((startY + endY) / 2.0)
			inputCentroids[i] = (cX, cY)
			inputDiameters[i] = endX - startX

		# if not tracking any objects take the input data and register each of them
		if len(self.objects) == 0:
			for i in range(0, len(inputCentroids)):
				self.register(inputCentroids[i], inputDiameters[i])

		# if tracking, try to match the input centroids to existing object centroids
		else:
			# grab the set of object IDs and corresponding centroids
			objectIDs = list(self.objects.keys())
			objectCentroids = list(self.objects.values())

			# compute the distance between each pair of object centroids and input centroids
			D = dist.cdist(np.array(objectCentroids), inputCentroids)

			# (1) find the smallest value in each row
			# (2) sort the row indexes based on their minimum values
			# (smallest value at the *front* of the index list
			rows = D.min(axis=1).argsort()

			# similar process on the columns
			cols = D.argmin(axis=1)[rows]

			# in order to determine if we need to update, register, or deregister an object,
			# keep track of which of the rows and column indexes are already examined
			usedRows = set()
			usedCols = set()

			# loop over the combination of the (row, column) index tuples
			for (row, col) in zip(rows, cols):
				# if already examined either row or column before, ignore it
				if row in usedRows or col in usedCols:
					continue

				# otherwise, grab object ID for the current row, set new centroid
				# and reset the disappeared counter
				objectID = objectIDs[row]
				self.objects[objectID] = inputCentroids[col]
				self.diameters[objectID] = inputDiameters[col]
				self.disappeared[objectID] = 0

				# indicate that each of the row and column indexes were examined
				usedRows.add(row)
				usedCols.add(col)

			# compute both the row and column index NOT yet examined
			unusedRows = set(range(0, D.shape[0])).difference(usedRows)
			unusedCols = set(range(0, D.shape[1])).difference(usedCols)

			# if number of object centroids is equal or greater than number of input centroids
			# check and see if some of these objects have potentially disappeared
			if D.shape[0] >= D.shape[1]:
				# loop over the unused row indexes
				for row in unusedRows:
					# grab the object ID for the corresponding row
					# index and increment the disappeared counter
					objectID = objectIDs[row]
					self.disappeared[objectID] += 1

					# check to see if object should be deregistered
					if self.disappeared[objectID] > self.maxDisappeared:
						self.deregister(objectID)

			# if number of input centroids is greater than the number of existing object centroids
			# register each new input centroid as a trackable object
			else:
				for col in unusedCols:
					self.register(inputCentroids[col], inputDiameters[col])

		# return the set of trackable objects
		return self.objects

	def updateInterpolation(self, rects):
		self.analyzedFrames += 1
		if not self.lastPosition:
			self.lastPosition = self.objects.copy()

		# check to see if the list of input bounding box rectangles is empty
		if len(rects) == 0:
			# loop over any existing tracked objects and mark them as disappeared
			for objectID in list(self.disappeared.keys()):
				self.disappeared[objectID] += 1

				# deregister if object disappeared for more frames than maxDisappeared
				if self.disappeared[objectID] > self.maxDisappeared:
					self.deregister(objectID)

			# return (no features to extract from empty list)
			return self.objects

		# initialize an array of input centroids and diameters for the current frame
		inputCentroids = np.zeros((len(rects), 2), dtype="int")
		inputDiameters = np.zeros((len(rects), 1), dtype="int")

		# loop over the bounding box rectangles
		for (i, (startX, startY, endX, endY)) in enumerate(rects):
			# use the bounding box coordinates to derive the centroid and diameter
			cX = int((startX + endX) / 2.0)
			cY = int((startY + endY) / 2.0)
			inputCentroids[i] = (cX, cY)
			inputDiameters[i] = endX - startX

		# if not tracking any objects take the input data and register each of them
		if len(self.objects) == 0:
			for i in range(0, len(inputCentroids)):
				self.register(inputCentroids[i], inputDiameters[i])

		# if tracking, try to match the input centroids to existing object centroids
		else:
			# grab the set of object IDs and corresponding centroids
			objectIDs = list(self.estimatedPosition.keys())
			objectCentroids = list(self.estimatedPosition.values())

			# compute the distance between each pair of object centroids and input centroids
			D = dist.cdist(np.array(objectCentroids), inputCentroids)

			# (1) find the smallest value in each row
			# (2) sort the row indexes based on their minimum values
			# (smallest value at the *front* of the index list
			rows = D.min(axis=1).argsort()

			# similar process on the columns
			cols = D.argmin(axis=1)[rows]

			# in order to determine if we need to update, register, or deregister an object,
			# keep track of which of the rows and column indexes are already examined
			usedRows = set()
			usedCols = set()

			# loop over the combination of the (row, column) index tuples
			for (row, col) in zip(rows, cols):
				# if already examined either row or column before, ignore it
				if row in usedRows or col in usedCols:
					continue

				# otherwise, grab object ID for the current row, set new centroid
				# and reset the disappeared counter
				objectID = objectIDs[row]
				self.objects[objectID] = inputCentroids[col]
				self.diameters[objectID] = inputDiameters[col]
				self.disappeared[objectID] = 0

				# indicate that each of the row and column indexes were examined
				usedRows.add(row)
				usedCols.add(col)

			# compute both the row and column index NOT yet examined
			unusedRows = set(range(0, D.shape[0])).difference(usedRows)
			unusedCols = set(range(0, D.shape[1])).difference(usedCols)

			# if number of object centroids is equal or greater than number of input centroids
			# check and see if some of these objects have potentially disappeared
			if D.shape[0] >= D.shape[1]:
				# loop over the unused row indexes
				for row in unusedRows:
					# grab the object ID for the corresponding row
					# index and increment the disappeared counter
					objectID = objectIDs[row]
					self.disappeared[objectID] += 1

					# check to see if object should be deregistered
					if self.disappeared[objectID] > self.maxDisappeared:
						self.deregister(objectID)

			# if number of input centroids is greater than the number of existing object centroids
			# register each new input centroid as a trackable object
			else:
				for col in unusedCols:
					self.register(inputCentroids[col], inputDiameters[col])

		# return the set of trackable objects
		return self.objects

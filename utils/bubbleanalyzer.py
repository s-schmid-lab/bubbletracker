import cv2
import numpy as np

class bubbleanalyzer

    def __init__(self, img):
        self.img = img.copy()

        if len(img.shape) < 3:
            self.gray = img.copy()
        else:
            self.gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        # might add initial values for filtering / blurring

    def blurImage(method, ksize, x, y):
        # Blur Image to remove noise
        self.imgBlur = cv2.bilateralFilter(self.img, 9, 101, 101)
        return self.imgBlur
        # blur = cv2.GaussianBlur(gray, (9, 9), 0)

    def binarizeImage(blocksize, C):
        # Adaptive Thresholding to create binary image
        binary = cv2.adaptiveThreshold(self.blurImage, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, blockSize=blocksize,
                                       C=C)

        # Performing morphological operation to remove noise, using kernel with disk shape as bubbles are elliptical to round
        kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5))

        # Previous working:
        # kernel = np.ones((3, 3), np.uint8)
        morph = cv2.morphologyEx(binary, cv2.MORPH_OPEN, kernel)

        # Previous: uncomment dilate operation
        kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (7, 7))
        morph = cv2.dilate(morph, kernel, iterations=1)
        morph = cv2.erode(morph, kernel, iterations=1)

        # Copy the threshold image for floodfilling
        im_floodfill = morph.copy()

        h, w = morph.shape[:2]
        mask = np.zeros((h + 2, w + 2), np.uint8)

        # Floodfill from (0, 0)
        cv2.floodFill(im_floodfill, mask, (0, 0), 255)

        # Invert floofilled image
        im_floodfill_inv = cv2.bitwise_not(im_floodfill)

        self.imgBinary = (morph | im_floodfill_inv)

        return self.Binary

    def getContours():
        self.contours, self.hierarchy = cv2.findContours(self.binary, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        return self.contours, self.hierarchy